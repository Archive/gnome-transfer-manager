/*
 *  Authors: Rodney Dawes <dobey@ximian.com>
 *
 *  Copyright 2003 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <bonobo.h>
#include <gnome.h>

#include <config.h>

#define GTM_OAFIID "OAFIID:GNOME_Transfer_Manager"

#include "GnomeTransferManager.h"

static gint test_transfer_idle_callback (void * data) {
  CORBA_Environment ev;
  CORBA_Object cobject;
  gchar ** argv = (gchar **) data;

  CORBA_exception_init (&ev);

  cobject = bonobo_activation_activate_from_id (GTM_OAFIID, 0, NULL, &ev);
  if (ev._major != CORBA_NO_EXCEPTION || cobject == CORBA_OBJECT_NIL) {
    CORBA_exception_free (&ev);
    bonobo_main_quit ();
    return FALSE;
  }

  GNOME_Transfer_Manager_save_uri_as (cobject, argv[1], argv[2], &ev);

  CORBA_exception_free (&ev);

  bonobo_main_quit ();

  return FALSE;
}

gint main (gint argc, gchar * argv[]) {
  CORBA_ORB orb;
  GError * gconf_error = NULL;
  GnomeProgram * program;

#ifdef ENABLE_NLS
  bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);
#endif

  program = gnome_program_init ("gnome-transfer-test", VERSION,
				LIBGNOME_MODULE,
				argc, argv, GNOME_PARAM_POPT_TABLE,
				NULL, NULL);

  if (!bonobo_is_initialized ()) {
    bonobo_init (&argc, argv);
  }

  if (!bonobo_activation_is_initialized ()) {
    orb = bonobo_activation_init (argc, argv);
  } else {
    orb = bonobo_activation_orb_get ();
  }

  g_idle_add (test_transfer_idle_callback, argv);
  bonobo_main ();

  return 0;
}


/*
 *  Authors: Rodney Dawes <dobey@ximian.com>
 *
 *  Copyright 2003 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include "g-transfer-manager.h"

void g_transfer_manager_queue_uri (GTransferManager * manager,
				   const gchar * source,
				   const gchar * target) {
  GTransferManagerItem * item;

  item = g_new0 (GTransferManagerItem, 1);

  item->source_uri = gnome_vfs_uri_new (source);
  item->target_uri = gnome_vfs_uri_new (target);

  //  manager->queue = g_list_append (manager->queue, item);

  g_transfer_manager_save_uri (manager, item);
}


/*
 *  Authors: Rodney Dawes <dobey@ximian.com>
 *
 *  Copyright 2003 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include "g-transfer-manager.h"

struct _GTransferManagerPriv {
  gchar * iid;
};

static void g_transfer_manager_finalize (GObject * object);

static void g_tm_impl_save_uri_as (PortableServer_Servant servant,
				   const CORBA_char * source,
				   const CORBA_char * target,
				   CORBA_Environment * ev) {
  GTransferManager * manager;

  manager = G_TRANSFER_MANAGER (bonobo_object_from_servant (servant));

  g_transfer_manager_queue_uri (manager, source, target);

  bonobo_object_unref (BONOBO_OBJECT (manager));
}

BONOBO_CLASS_BOILERPLATE_FULL (GTransferManager, g_transfer_manager,
			       GNOME_Transfer_Manager, BonoboObject,
			       BONOBO_TYPE_OBJECT)

static void g_transfer_manager_class_init (GTransferManagerClass * klass) {
  GObjectClass * object_class;
  POA_GNOME_Transfer_Manager__epv * epv;

  parent_class = g_type_class_peek (BONOBO_TYPE_OBJECT);
  object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = g_transfer_manager_finalize;

  epv = &klass->epv;
  epv->save_uri_as = g_tm_impl_save_uri_as;
}

static void g_transfer_manager_instance_init (GTransferManager * manager) {
  GTransferManagerPriv * priv;

  priv = g_new0 (GTransferManagerPriv, 1);
  priv->iid = NULL;

  manager->priv = priv;
  manager->client = gconf_client_get_default ();
  manager->queue = NULL;
}

static gboolean g_transfer_manager_construct (GTransferManager * manager,
					      const gchar * iid) {
  GTransferManagerPriv * priv;
  CORBA_Object corba_object;
  Bonobo_RegistrationResult result;

  g_return_val_if_fail (manager != NULL, FALSE);

  corba_object = bonobo_object_corba_objref (BONOBO_OBJECT (manager));

  result = bonobo_activation_register_active_server (iid, corba_object,
						     NULL);

  if (result != Bonobo_ACTIVATION_REG_SUCCESS) {
    return FALSE;
  }

  priv = manager->priv;

  priv->iid = g_strdup (iid);

  return TRUE;
}

static void g_transfer_manager_finalize (GObject * object) {
  GTransferManager * manager = G_TRANSFER_MANAGER (object);
  GTransferManagerPriv * priv;
  GList * list;

  g_object_unref (manager->client);

  priv = manager->priv;

  g_free (priv->iid);
  g_free (priv);

  for (list = manager->queue; list != NULL; list = list->next) {
    GTransferManagerItem * item = list->data;

    manager->queue = g_list_remove (manager->queue, item);
  }
  g_list_free (list);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static GTransferManager * g_transfer_manager_new (void) {
  GTransferManager * new;

  new = g_object_new (G_TYPE_TRANSFER_MANAGER, NULL);

  if (!g_transfer_manager_construct (new, "OAFIID:GNOME_Transfer_Manager")) {
    bonobo_object_unref (BONOBO_OBJECT (new));
    return NULL;
  }

  return new;
}

static GTransferManager * gtman = NULL;

static gint g_transfer_manager_idle_callback (void * data) {
  CORBA_Environment ev;
  CORBA_Object cobject;
  
  CORBA_exception_init (&ev);

  gtman = g_transfer_manager_new ();
  if (gtman == NULL) {
    cobject = bonobo_activation_activate_from_id ("OAFIID:GNOME_Transfer_Manager",
						  0, NULL, &ev);
    if (ev._major != CORBA_NO_EXCEPTION || cobject == CORBA_OBJECT_NIL) {
      CORBA_exception_free (&ev);
      bonobo_main_quit ();
      return FALSE;
    }
  } else {
    cobject = bonobo_object_corba_objref (BONOBO_OBJECT (gtman));
    cobject = CORBA_Object_duplicate (cobject, &ev);
    Bonobo_Unknown_ref (cobject, &ev);
  }

  CORBA_exception_free (&ev);

  if (gtman == NULL) {
    bonobo_main_quit ();
  }

  return FALSE;
}

gint main (gint argc, gchar * argv[]) {
  CORBA_ORB orb;
  GError * gconf_error = NULL;
  GnomeProgram * program;

#ifdef ENABLE_NLS
  bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);
#endif

  program = gnome_program_init ("gnome-transfer-manager", VERSION,
				LIBGNOME_MODULE,
				argc, argv, GNOME_PARAM_POPT_TABLE,
				NULL, NULL);

  if (!bonobo_is_initialized ()) {
    bonobo_init (&argc, argv);
  }

  if (!bonobo_activation_is_initialized ()) {
    orb = bonobo_activation_init (argc, argv);
  } else {
    orb = bonobo_activation_orb_get ();
  }

  if (!gconf_init (argc, argv, &gconf_error)) {
    g_assert (gconf_error != NULL);
    g_error ("GConf init failed:\n  %s", gconf_error->message);
    return FALSE;
  }

  gnome_vfs_init ();

  g_idle_add (g_transfer_manager_idle_callback, NULL);
  bonobo_main ();

  return 0;
}


/*
 *  Authors: Rodney Dawes <dobey@ximian.com>
 *
 *  Copyright 2003 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef _GNOME_TRANSFER_SERVER_H_
#define _GNOME_TRANSFER_SERVER_H_

#include <bonobo.h>
#include <gnome.h>

#include <libgnomevfs/gnome-vfs.h>
#include <gconf/gconf-client.h>

#include <config.h>

#include "GnomeTransferManager.h"

typedef struct _GTransferManager GTransferManager;
typedef struct _GTransferManagerPriv GTransferManagerPriv;
typedef struct _GTransferManagerClass GTransferManagerClass;

#include "g-transfer-manager-item.h"
#include "g-transfer-manager-queue.h"
#include "g-transfer-manager-vfs.h"

G_BEGIN_DECLS

struct _GTransferManager {
  BonoboObject parent;
  GTransferManagerPriv * priv;

  GConfClient * client;

  GList * queue;
};

struct _GTransferManagerClass {
  BonoboObjectClass parent_class;
  POA_GNOME_Transfer_Manager__epv epv;

  /* Padding for future expansion */
  void (*_e_reserved1) (void);
  void (*_e_reserved2) (void);
  void (*_e_reserved3) (void);
  void (*_e_reserved4) (void);
};

#define G_TYPE_TRANSFER_MANAGER (g_transfer_manager_get_type())
#define G_TRANSFER_MANAGER(obj) (G_TYPE_CHECK_INSTANCE_CAST (obj, G_TYPE_TRANSFER_MANAGER, GTransferManager))
#define G_TRANSFER_MANAGER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST (obj, G_TYPE_TRANSFER_MANAGER, GTransferManagerClass))
#define G_IS_TRANSFER_MANAGER(obj) (G_TYPE_CHECK_INSTANCE_TYPE (obj, G_TYPE_TRANSFER_MANAGER))
#define G_IS_TRANSFER_MANAGER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE (obj, G_TYPE_TRANSFER_MANAGER))
#define G_TRANSFER_MANAGER_GET_CLASS(obj) (G_TYPE_CHECK_GET_CLASS (obj, G_TYPE_TRANSFER_MANAGER, GTransferManagerClass))

GType g_transfer_manager_get_type (void);

G_END_DECLS

#endif

/*
 *  Authors: Rodney Dawes <dobey@ximian.com>
 *
 *  Copyright 2003 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include "g-transfer-manager.h"

typedef struct {
  GTransferManager * manager;
  GTransferManagerItem * item;
} GTransferManagerPItem;

static void g_transfer_manager_vfs_progress (GnomeVFSAsyncHandle * handle,
					     GnomeVFSXferProgressInfo * info,
					     GTransferManagerPItem * pi) {
  g_print ("DEBUG: Phase: %d\n", info->phase);
  switch (info->phase) {
  case GNOME_VFS_XFER_PHASE_COPYING: {
      g_print ("DEBUG: %s\n", gnome_vfs_result_to_string (info->vfs_status));
    if (info->vfs_status != GNOME_VFS_OK) {
      g_print ("DEBUG: %s\n", gnome_vfs_result_to_string (info->vfs_status));
    }
    break;
  }
  case GNOME_VFS_XFER_PHASE_COMPLETED:
  case GNOME_VFS_XFER_PHASE_CLOSETARGET: {
    g_print ("Download complete\n");
    //    pi->manager->queue = g_list_remove (pi->manager->queue, pi->item);
    break;
  }
  default:
    break;
  }
}

void g_transfer_manager_save_uri (GTransferManager * manager,
				  GTransferManagerItem * item) {
  GTransferManagerPItem * pi;
  GList * sources;
  GList * targets;

  pi = g_new0 (GTransferManagerPItem, 1);
  
  g_print ("DEBUG: %s -> %s\n",
	   gnome_vfs_uri_to_string (item->source_uri, GNOME_VFS_URI_HIDE_NONE),
	   gnome_vfs_uri_to_string (item->target_uri, GNOME_VFS_URI_HIDE_NONE)
	   );

  sources = g_list_append (NULL, item->source_uri);
  targets = g_list_append (NULL, item->target_uri);

  pi->manager = manager;
  pi->item = item;

  gnome_vfs_async_xfer (&item->handle, sources, targets,
			GNOME_VFS_XFER_DEFAULT,
			GNOME_VFS_XFER_ERROR_MODE_ABORT,
			GNOME_VFS_XFER_OVERWRITE_MODE_REPLACE,
			GNOME_VFS_PRIORITY_DEFAULT,
			(GnomeVFSAsyncXferProgressCallback)
			g_transfer_manager_vfs_progress,
			pi, NULL, NULL);
}

